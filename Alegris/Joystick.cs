﻿using System.Windows.Forms;

namespace Alegris
{
    static class Joystick
    {
        public static Keys Rotate;
        public static Keys Down;
        public static Keys Left;
        public static Keys Right;

        static Joystick()
        {
            Rotate = Keys.Up;
            Down = Keys.Down;
            Left = Keys.Left;
            Right = Keys.Right;
        }
    }
}
