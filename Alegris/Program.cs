using System;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;
using System.Media;

namespace Alegris
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            SoundPlayer sound = new SoundPlayer(@"C:\Users\surya\Documents\GitHub\alegris\Alegris\Resources\TetrisMusic.wav");
            sound.PlayLooping();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            new GameApp().Run(args);
        }
    }

    public class GameApp : WindowsFormsApplicationBase
    {
        protected override void OnCreateSplashScreen()
        {
            SplashScreen = new SplashScreen();
            base.OnCreateSplashScreen();
        }

        protected override void OnCreateMainForm()
        {
            MainForm = new AlegrisGame();
            base.OnCreateMainForm();
        }
    }
}