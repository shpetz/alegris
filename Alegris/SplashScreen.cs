﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Alegris
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();

            this.BackColor = Color.FromArgb(236, 236, 236);
            this.TransparencyKey = Color.FromArgb(236, 236, 236);
        }
    }
}
