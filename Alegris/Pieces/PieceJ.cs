using System.Drawing;
using Alegris.Enumerations;

namespace Alegris
{
    public class PieceJ : Piece
    {
        public PieceJ()
        {
            Dimention = new Point(2, 3);
            Position = new Point(4, 0);
            Shape = new PieceShape[Dimention.X, Dimention.Y];
            Shape[0, 0] = PieceShape.None;
            Shape[0, 1] = PieceShape.None;
            Shape[0, 2] = PieceShape.J;
            Shape[1, 0] = PieceShape.J;
            Shape[1, 1] = PieceShape.J;
            Shape[1, 2] = PieceShape.J;
        }
    }
}