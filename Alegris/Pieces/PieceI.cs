using System.Drawing;
using Alegris.Enumerations;

namespace Alegris
{
    public class PieceI : Piece
    {
        public PieceI()
        {
            Dimention = new Point(1, 4);
            Position = new Point(5, 0);
            Shape = new PieceShape[Dimention.X, Dimention.Y];
            Shape[0, 0] = PieceShape.I;
            Shape[0, 1] = PieceShape.I;
            Shape[0, 2] = PieceShape.I;
            Shape[0, 3] = PieceShape.I;
        }

        public override void Rotate()
        {
            base.Rotate();

            if (Dimention.X > Dimention.Y)
            {
                Position.X -= 2;
            }
            else
            {
                Position.X += 2;
            }
        }
    }
}