using System.Drawing;
using Alegris.Enumerations;

namespace Alegris
{
    public class PieceO : Piece
    {
        public PieceO()
        {
            Dimention = new Point(2, 2);
            Position = new Point(4, 0);
            Shape = new PieceShape[Dimention.X, Dimention.Y];
            Shape[0, 0] = PieceShape.O;
            Shape[0, 1] = PieceShape.O;
            Shape[1, 0] = PieceShape.O;
            Shape[1, 1] = PieceShape.O;
        }
    }
}