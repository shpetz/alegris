using System;
using System.Drawing;
using Alegris.Enumerations;

namespace Alegris
{
    public abstract class Piece : ICloneable
    {
        public Point Dimention;
        public Point Position;
        public PieceShape[,] Shape;

        public virtual void Rotate()
        {
            var newShape = new PieceShape[Dimention.Y, Dimention.X];

            for (int i = 0; i < Dimention.X; i++)
            {
                for (int j = 0; j < Dimention.Y; j++)
                {
                    newShape[j, i] = Shape[i, (Dimention.Y - 1) - j];
                }
            }

            Shape = newShape;

            int t = Dimention.X;
            Dimention.X = Dimention.Y;
            Dimention.Y = t;
        }

        #region ICloneable Members

        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion
    }
}
