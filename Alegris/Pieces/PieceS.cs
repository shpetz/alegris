﻿using System.Drawing;
using Alegris.Enumerations;

namespace Alegris
{
    public class PieceS : Piece
    {
        public PieceS()
        {
            Dimention = new Point(3, 2);
            Position = new Point(4, 0);
            Shape = new PieceShape[Dimention.X, Dimention.Y];
            Shape[0, 0] = PieceShape.None;
            Shape[0, 1] = PieceShape.S;
            Shape[1, 0] = PieceShape.S;
            Shape[1, 1] = PieceShape.S;
            Shape[2, 0] = PieceShape.S;
            Shape[2, 1] = PieceShape.None;
        }
    }
}
