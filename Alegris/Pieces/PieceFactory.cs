using System;
using Alegris.Enumerations;

namespace Alegris
{
    class PieceFactory
    {
        public static Piece CreateRandomPiece()
        {
            PieceShape shape = default(PieceShape);

            var random = new Random(unchecked((int)DateTime.Now.Ticks));
            int index = random.Next(0, 6);

            switch (index)
            {
                case 0: shape = PieceShape.I; break;
                case 1: shape = PieceShape.J; break;
                case 2: shape = PieceShape.L; break;
                case 3: shape = PieceShape.O; break;
                case 4: shape = PieceShape.S; break;
                case 5: shape = PieceShape.T; break;
                case 6: shape = PieceShape.Z; break;
            }

            return CreatePiece(shape);
        }

        private static Piece CreatePiece(PieceShape shape)
        {
            switch (shape)
            {
                case PieceShape.I: return new PieceI();
                case PieceShape.J: return new PieceJ();
                case PieceShape.L: return new PieceL();
                case PieceShape.O: return new PieceO();
                case PieceShape.S: return new PieceS();
                case PieceShape.T: return new PieceT();
                case PieceShape.Z: return new PieceZ();
            }

            return null;
        }
    }
}
