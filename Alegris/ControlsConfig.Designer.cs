﻿namespace Alegris
{
    partial class ControlsConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtRight = new System.Windows.Forms.TextBox();
            this.txtLeft = new System.Windows.Forms.TextBox();
            this.txtDown = new System.Windows.Forms.TextBox();
            this.lblRight = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblDown = new System.Windows.Forms.Label();
            this.txtRotate = new System.Windows.Forms.TextBox();
            this.lblUp = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 240);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(160, 240);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Alegris.Properties.Resources.joystick;
            this.pictureBox1.Location = new System.Drawing.Point(98, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // txtRight
            // 
            this.txtRight.Location = new System.Drawing.Point(232, 68);
            this.txtRight.Name = "txtRight";
            this.txtRight.ReadOnly = true;
            this.txtRight.Size = new System.Drawing.Size(80, 20);
            this.txtRight.TabIndex = 4;
            this.txtRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtKeyDown);
            // 
            // txtLeft
            // 
            this.txtLeft.Location = new System.Drawing.Point(12, 68);
            this.txtLeft.Name = "txtLeft";
            this.txtLeft.ReadOnly = true;
            this.txtLeft.Size = new System.Drawing.Size(80, 20);
            this.txtLeft.TabIndex = 5;
            this.txtLeft.Tag = "";
            this.txtLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLeft.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtKeyDown);
            // 
            // txtDown
            // 
            this.txtDown.Location = new System.Drawing.Point(122, 148);
            this.txtDown.Name = "txtDown";
            this.txtDown.ReadOnly = true;
            this.txtDown.Size = new System.Drawing.Size(80, 20);
            this.txtDown.TabIndex = 6;
            this.txtDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtKeyDown);
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Location = new System.Drawing.Point(256, 52);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(32, 13);
            this.lblRight.TabIndex = 8;
            this.lblRight.Text = "Right";
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Location = new System.Drawing.Point(40, 52);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(25, 13);
            this.lblLeft.TabIndex = 9;
            this.lblLeft.Text = "Left";
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Location = new System.Drawing.Point(145, 171);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(35, 13);
            this.lblDown.TabIndex = 10;
            this.lblDown.Text = "Down";
            // 
            // txtRotate
            // 
            this.txtRotate.Location = new System.Drawing.Point(54, 205);
            this.txtRotate.Name = "txtRotate";
            this.txtRotate.ReadOnly = true;
            this.txtRotate.Size = new System.Drawing.Size(80, 20);
            this.txtRotate.TabIndex = 3;
            this.txtRotate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRotate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtKeyDown);
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Location = new System.Drawing.Point(9, 208);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(39, 13);
            this.lblUp.TabIndex = 7;
            this.lblUp.Text = "Rotate";
            // 
            // ControlsConfig
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(328, 275);
            this.Controls.Add(this.lblDown);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblUp);
            this.Controls.Add(this.txtDown);
            this.Controls.Add(this.txtLeft);
            this.Controls.Add(this.txtRight);
            this.Controls.Add(this.txtRotate);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ControlsConfig";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Setup Controls";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtRight;
        private System.Windows.Forms.TextBox txtLeft;
        private System.Windows.Forms.TextBox txtDown;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.TextBox txtRotate;
        private System.Windows.Forms.Label lblUp;
    }
}