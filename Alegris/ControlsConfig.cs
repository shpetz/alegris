﻿using System;
using System.Windows.Forms;

namespace Alegris
{
    public partial class ControlsConfig : Form
    {
        public ControlsConfig()
        {
            InitializeComponent();

            txtDown.Text = Joystick.Down.ToString();
            txtDown.Tag = Joystick.Down;

            txtLeft.Text = Joystick.Left.ToString();
            txtLeft.Tag = Joystick.Left;

            txtRight.Text = Joystick.Right.ToString();
            txtRight.Tag = Joystick.Right;

            txtRotate.Text = Joystick.Rotate.ToString();
            txtRotate.Tag = Joystick.Rotate;
        }

        private void TxtKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.Text = e.KeyData.ToString();
            textBox.Tag = e.KeyData;
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            Joystick.Rotate = (Keys)txtRotate.Tag;
            Joystick.Left = (Keys)txtLeft.Tag;
            Joystick.Right = (Keys)txtRight.Tag;
            Joystick.Rotate = (Keys)txtRotate.Tag;
            Close();
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
