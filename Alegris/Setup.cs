using System.Drawing;
using System.Collections.Generic;
using Alegris.Enumerations;

namespace Alegris
{
    class Setup
    {
        public static int Cols = 10;
        public static int Rows = 20;

        public static int BlockSize = 20;

        public static int BoardMarginLeft = 13;
        public static int BoardMarginTop = 35;
        public static int BoardWidth = 200;
        public static int BoardHeight = 400;

        public static int NextPiecePanelLeft = 216;
        public static int NextPiecePanelTop = 35;
        public static int NextPieceWidth = 117;
        public static int NextPiecePanelHeight = 100;

        public static Dictionary<PieceShape, Color> PieceColors = new Dictionary<PieceShape, Color>
        {
            { PieceShape.I, Color.Red },
            { PieceShape.J, Color.Pink },
            { PieceShape.L, Color.Yellow },
            { PieceShape.O, Color.LightBlue },
            { PieceShape.S, Color.Blue },
            { PieceShape.T, Color.Gray },
            { PieceShape.Z, Color.Green },
            { PieceShape.None, Color.Black }
        };

        public static Color GetPieceColor(PieceShape piece)
        {
            return PieceColors[piece];
        }

        public static Bitmap GetPieceBitmap(PieceShape piece)
        {
            switch (piece)
            {
                case PieceShape.I: return Properties.Resources.I;
                case PieceShape.J: return Properties.Resources.J;
                case PieceShape.L: return Properties.Resources.L;
                case PieceShape.O: return Properties.Resources.O;
                case PieceShape.S: return Properties.Resources.S;
                case PieceShape.T: return Properties.Resources.T;
                case PieceShape.Z: return Properties.Resources.Z;
            }

            return Properties.Resources.I;
        }
    }
}
