namespace Alegris.Enumerations
{
    public enum PieceShape
    {
        I,
        J,
        L,
        O,
        S,
        T,
        Z,
        None
    }
}
