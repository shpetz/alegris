namespace Alegris.Enumerations
{
    public enum PieceColor
    {
        I,
        J,
        L,
        O,
        S,
        T,
        Z,
        None
    }
}
