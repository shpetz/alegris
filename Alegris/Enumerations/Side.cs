namespace Alegris.Enumerations
{
    public enum Side
    {
        Down,
        Left,
        Right,
        Up,
    }
}
