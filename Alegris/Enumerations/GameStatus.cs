namespace Alegris.Enumerations
{
    public enum GameStatus
    {
        GameOver,
        Paused,
        Running,
        Stoped
    }
}
