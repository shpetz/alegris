using System;

namespace Alegris
{
    partial class AlegrisGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joystickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.themesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theme6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.useStyledPiecesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelLines = new System.Windows.Forms.Label();
            this.labelScoreShow = new System.Windows.Forms.Label();
            this.labelLevelShow = new System.Windows.Forms.Label();
            this.labelLinesShow = new System.Windows.Forms.Label();
            this.imgPaused = new System.Windows.Forms.PictureBox();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPaused)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configToolStripMenuItem,
            this.themesToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(344, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.pauseToolStripMenuItem,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.fileToolStripMenuItem.Text = "&Game";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.newGameToolStripMenuItem.Text = "&New";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.NewGameToolStripMenuItemClick);
            // 
            // pauseToolStripMenuItem
            // 
            this.pauseToolStripMenuItem.Name = "pauseToolStripMenuItem";
            this.pauseToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.pauseToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.pauseToolStripMenuItem.Text = "&Pause";
            this.pauseToolStripMenuItem.Click += new System.EventHandler(this.PauseToolStripMenuItemClick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(121, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItemClick);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.joystickToolStripMenuItem});
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.configToolStripMenuItem.Text = "&Config";
            // 
            // joystickToolStripMenuItem
            // 
            this.joystickToolStripMenuItem.Name = "joystickToolStripMenuItem";
            this.joystickToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.joystickToolStripMenuItem.Text = "&Joystick";
            this.joystickToolStripMenuItem.Click += new System.EventHandler(this.SetupJoystickToolStripMenuItemClick);
            // 
            // themesToolStripMenuItem
            // 
            this.themesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.theme1ToolStripMenuItem,
            this.theme2ToolStripMenuItem,
            this.theme3ToolStripMenuItem,
            this.theme4ToolStripMenuItem,
            this.theme5ToolStripMenuItem,
            this.theme6ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.useStyledPiecesToolStripMenuItem});
            this.themesToolStripMenuItem.Name = "themesToolStripMenuItem";
            this.themesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.themesToolStripMenuItem.Text = "&Themes";
            // 
            // theme1ToolStripMenuItem
            // 
            this.theme1ToolStripMenuItem.Checked = true;
            this.theme1ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.theme1ToolStripMenuItem.Name = "theme1ToolStripMenuItem";
            this.theme1ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme1ToolStripMenuItem.Tag = "1";
            this.theme1ToolStripMenuItem.Text = "Theme &1";
            this.theme1ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // theme2ToolStripMenuItem
            // 
            this.theme2ToolStripMenuItem.Name = "theme2ToolStripMenuItem";
            this.theme2ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme2ToolStripMenuItem.Tag = "2";
            this.theme2ToolStripMenuItem.Text = "Theme &2";
            this.theme2ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // theme3ToolStripMenuItem
            // 
            this.theme3ToolStripMenuItem.Name = "theme3ToolStripMenuItem";
            this.theme3ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme3ToolStripMenuItem.Tag = "3";
            this.theme3ToolStripMenuItem.Text = "Theme &3";
            this.theme3ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // theme4ToolStripMenuItem
            // 
            this.theme4ToolStripMenuItem.Name = "theme4ToolStripMenuItem";
            this.theme4ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme4ToolStripMenuItem.Tag = "4";
            this.theme4ToolStripMenuItem.Text = "Theme &4";
            this.theme4ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // theme5ToolStripMenuItem
            // 
            this.theme5ToolStripMenuItem.Name = "theme5ToolStripMenuItem";
            this.theme5ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme5ToolStripMenuItem.Tag = "5";
            this.theme5ToolStripMenuItem.Text = "Theme &5";
            this.theme5ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // theme6ToolStripMenuItem
            // 
            this.theme6ToolStripMenuItem.Name = "theme6ToolStripMenuItem";
            this.theme6ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.theme6ToolStripMenuItem.Tag = "6";
            this.theme6ToolStripMenuItem.Text = "Theme &6";
            this.theme6ToolStripMenuItem.Click += new System.EventHandler(this.SkinToolStripMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(162, 6);
            // 
            // useStyledPiecesToolStripMenuItem
            // 
            this.useStyledPiecesToolStripMenuItem.Name = "useStyledPiecesToolStripMenuItem";
            this.useStyledPiecesToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.useStyledPiecesToolStripMenuItem.Text = "Use Styled Blocks";
            this.useStyledPiecesToolStripMenuItem.Click += new System.EventHandler(this.UseOctahedronToolStripMenuItemClick);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItemClick);
            // 
            // statusStrip
            // 
            this.statusStrip.Location = new System.Drawing.Point(0, 446);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(344, 22);
            this.statusStrip.TabIndex = 1;
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.BackColor = System.Drawing.Color.Transparent;
            this.labelScore.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.ForeColor = System.Drawing.Color.Black;
            this.labelScore.Location = new System.Drawing.Point(228, 164);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(62, 18);
            this.labelScore.TabIndex = 4;
            this.labelScore.Text = "Score:";
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.BackColor = System.Drawing.Color.Transparent;
            this.labelLevel.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelLevel.ForeColor = System.Drawing.Color.Black;
            this.labelLevel.Location = new System.Drawing.Point(228, 189);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(59, 18);
            this.labelLevel.TabIndex = 5;
            this.labelLevel.Text = "Level:";
            // 
            // labelLines
            // 
            this.labelLines.AutoSize = true;
            this.labelLines.BackColor = System.Drawing.Color.Transparent;
            this.labelLines.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold);
            this.labelLines.ForeColor = System.Drawing.Color.Black;
            this.labelLines.Location = new System.Drawing.Point(228, 216);
            this.labelLines.Name = "labelLines";
            this.labelLines.Size = new System.Drawing.Size(57, 18);
            this.labelLines.TabIndex = 6;
            this.labelLines.Text = "Lines:";
            // 
            // labelScoreShow
            // 
            this.labelScoreShow.AutoSize = true;
            this.labelScoreShow.BackColor = System.Drawing.Color.Transparent;
            this.labelScoreShow.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScoreShow.ForeColor = System.Drawing.Color.Black;
            this.labelScoreShow.Location = new System.Drawing.Point(290, 164);
            this.labelScoreShow.Name = "labelScoreShow";
            this.labelScoreShow.Size = new System.Drawing.Size(17, 18);
            this.labelScoreShow.TabIndex = 7;
            this.labelScoreShow.Text = "0";
            // 
            // labelLevelShow
            // 
            this.labelLevelShow.AutoSize = true;
            this.labelLevelShow.BackColor = System.Drawing.Color.Transparent;
            this.labelLevelShow.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLevelShow.ForeColor = System.Drawing.Color.Black;
            this.labelLevelShow.Location = new System.Drawing.Point(290, 189);
            this.labelLevelShow.Name = "labelLevelShow";
            this.labelLevelShow.Size = new System.Drawing.Size(17, 18);
            this.labelLevelShow.TabIndex = 8;
            this.labelLevelShow.Text = "1";
            // 
            // labelLinesShow
            // 
            this.labelLinesShow.AutoSize = true;
            this.labelLinesShow.BackColor = System.Drawing.Color.Transparent;
            this.labelLinesShow.Font = new System.Drawing.Font("Jing Jing", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLinesShow.ForeColor = System.Drawing.Color.Black;
            this.labelLinesShow.Location = new System.Drawing.Point(290, 216);
            this.labelLinesShow.Name = "labelLinesShow";
            this.labelLinesShow.Size = new System.Drawing.Size(17, 18);
            this.labelLinesShow.TabIndex = 9;
            this.labelLinesShow.Text = "0";
            // 
            // imgPaused
            // 
            this.imgPaused.BackColor = System.Drawing.Color.Transparent;
            this.imgPaused.Image = global::Alegris.Properties.Resources.paused;
            this.imgPaused.Location = new System.Drawing.Point(14, 184);
            this.imgPaused.Name = "imgPaused";
            this.imgPaused.Size = new System.Drawing.Size(200, 100);
            this.imgPaused.TabIndex = 10;
            this.imgPaused.TabStop = false;
            this.imgPaused.Visible = false;
            // 
            // AlegrisGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Alegris.Properties.Resources.baseSkin1;
            this.ClientSize = new System.Drawing.Size(344, 468);
            this.Controls.Add(this.imgPaused);
            this.Controls.Add(this.labelLinesShow);
            this.Controls.Add(this.labelLevelShow);
            this.Controls.Add(this.labelScoreShow);
            this.Controls.Add(this.labelLines);
            this.Controls.Add(this.labelLevel);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "AlegrisGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alegris";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPaused)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelLines;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.Label labelScoreShow;
        private System.Windows.Forms.Label labelLevelShow;
        private System.Windows.Forms.Label labelLinesShow;
        private System.Windows.Forms.PictureBox imgPaused;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joystickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem themesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theme6ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem useStyledPiecesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

